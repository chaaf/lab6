#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) {
  return std::rand()%i;
}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));
  Card deck[52];
//incrementing through array and setting eac cards value and suit type
  for(int i = 0; i < 13; i++){
    deck[i].suit = SPADES;
    deck[i].value = i + 2;
  }
  for(int i = 13; i < 26; i++){
    deck[i].suit = HEARTS;
    deck[i].value = i - 11;
  }
  for(int i = 26; i < 39; i++){
    deck[i].suit = DIAMONDS;
    deck[i].value = i - 24;
  }
  for(int i = 39; i < 52; i++){
    deck[i].suit = CLUBS;
    deck[i].value = i - 37;
  }
//cout << deck[0].value << get_suit_code(deck[10]) << deck[34].value << get_suit_code(deck[50]);
  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/


  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   //creating file pointer to function myrandom
   int (*fp)(int);
   fp = myrandom;
   //randomly shuffling deck
   random_shuffle(deck, &deck[52], fp);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    //creating hand made up of first 5 elements of deck
    Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     //sorting hand in suit and value order
     sort(hand, &hand[5], suit_order);



    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     //printing hand
   for(int printer = 0; printer < 5; printer ++){
       cout << setw(10) << right << get_card_name(hand[printer]) << " of " << get_suit_code(hand[printer]) << endl;
    }




  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  bool order = true;
  //testing to see if suits are in order
  if(lhs.suit < rhs.suit){
    order = true;
  }
  //if suits are the same, checks value of card to determine if they are in order
  else if(lhs.suit == rhs.suit){
    if(lhs.value < rhs.value){
      order = true;
    }
    else if(lhs.value >= rhs.value){
      order = false;
    }
  }
  else if(lhs.suit > rhs.suit){
    order = false;
  }
  return order;
}
//given function
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  //returns card name based n value
  //all names equal to value except face cards
  string cardName;
  if(c.value == 2){
    cardName = "2";
  }
  else if(c.value == 3){
    cardName = "3";
  }
  else if(c.value == 4){
    cardName = "4";
  }
  else if(c.value == 5){
    cardName = "5";
  }
  else if(c.value == 6){
    cardName = "6";
  }
  else if(c.value == 7){
    cardName = "7";
  }
  else if(c.value == 8){
    cardName = "8";
  }
  else if(c.value == 9){
    cardName = "9";
  }
  else if(c.value == 10){
    cardName = "10";
  }
  else if(c.value == 11){
    cardName = "Jack";
  }
  else if(c.value == 12){
    cardName = "Queen";
  }
  else if(c.value == 13){
    cardName = "King";
  }
  else if(c.value == 14){
    cardName = "Ace";
  }
  return cardName;
}
